![](https://i.imgur.com/KSbsTd5.png)

## Back End Developer Challenge
This endpoint containing a json reponse of one course data [https://staticsiteslab.s3.ap-south-1.amazonaws.com/hiring/GameDev.json](https://staticsiteslab.s3.ap-south-1.amazonaws.com/hiring/GameDev.json)

Build an Backend to facilitate CRUD and Search Courses listings


## Requirements and Output
- functions for facilitate CRUD operations
- function to Display the total number of courses found


## Must to have
- Swagger documentation
- User Profile info
- OAuth Implemantation to achive authentication and authorization



####  Build a backend  application using one of the following:
- hapi
- Loopback (preferred)
- laravel,django etc...

## Conditions

- Do not use any lamda/cloud functions to create API endpoints
- If you make any assumptions while solving the exercise please mention them clearly in the readme file

## What we are looking for

- **Simple readable code** How well structured it is? Clear separation of concerns? Can anyone just look at it and get the idea to
what is being done? Does it follow any standards?
- **Correctness** Does the application do what it promises? Can we find bugs or trivial flaws?
- **Memory efficiency** How will it behave in case of large datasets?
- **Testing** How well tested your application is? Can you give some metrics?

## Questions & Delivery

If you have any questions to this challenge, please do reach out to us.

The challenge should be delivered as a link to a public git repository (gitlab.com or bitbucket.com or github are preferred).


## Checklist

Before submitting, make sure that your program

- [ ] Code accompanies the Unit Tests
- [ ] Deployment URL 
- [ ] API Documentation URL
- [ ] Usage is clearly mentioned in the README file, This including setup the project, how to run it, how to run unit test, examples,etc


## Note

Implementations focusing on **quality over feature completeness** will be highly appreciated,  don’t feel compelled to implement everything and even if you are not able to complete the challenge, please do submit it anyways.